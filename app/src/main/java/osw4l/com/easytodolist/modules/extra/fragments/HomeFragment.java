package osw4l.com.easytodolist.modules.extra.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import osw4l.com.easytodolist.MainActivity;
import osw4l.com.easytodolist.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        this.setActionBarTittle("Inicio");

        return view;
    }


    public void setActionBarTittle(String t){
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(
                t
        );
    }

}
