package osw4l.com.easytodolist.modules.extra.models;

import android.graphics.drawable.PictureDrawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringDef;

/**
 * Created by osw4l on 29/12/16.
 */

public class StartItem {

    private String title_, body_text, text_bottom;
    private int icon_button;
    private String background;
    private String color_1, color_2;

    public String getColor_1() {
        return color_1;
    }

    public String getColor_2() {
        return color_2;
    }

    public void setColor_1(String color_1) {
        this.color_1 = color_1;
    }

    public void setColor_2(String color_2) {
        this.color_2 = color_2;
    }

    public StartItem(String title_, int icon_button, String text_bottom, String body_text, String background, String color_1, String color_2) {
        this.title_ = title_;
        this.icon_button = icon_button;
        this.text_bottom = text_bottom;
        this.body_text = body_text;
        this.background = background;
        this.color_1 = color_1;
        this.color_2 = color_2;
    }

    public String getTitle_() {
        return title_;
    }

    public void setTitle_(String title_) {
        this.title_ = title_;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public int getIcon_button() {
        return icon_button;
    }

    public void setIcon_button(int icon_button) {
        this.icon_button = icon_button;
    }

    public String getText_bottom() {
        return text_bottom;
    }

    public void setText_bottom(String text_bottom) {
        this.text_bottom = text_bottom;
    }

    public String getBody_text() {
        return body_text;
    }

    public void setBody_text(String body_text) {
        this.body_text = body_text;
    }
}
