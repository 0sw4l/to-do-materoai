package osw4l.com.easytodolist.modules.todo.models;

import com.orm.SugarRecord;

import java.sql.Time;
import java.util.Date;

/**
 * Created by osw4l on 28/12/16.
 */

public class Todo extends SugarRecord {

    public String name;
    public Date date;
    public Time time;
    public double lng, lat;
    public boolean check;
    public boolean archived;
    public String detail;

    public Todo(String name, Date date, Time time, double lng, double lat, boolean check, boolean archived, String detail) {
        this.name = name;
        this.date = date;
        this.time = time;
        this.lng = lng;
        this.lat = lat;
        this.check = check;
        this.archived = archived;
        this.detail = detail;
    }

    public Todo(){

    }

    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }
}
