package osw4l.com.easytodolist.modules.categories.models;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

public class Category extends SugarRecord {

    public String name;
    public int notes;

    public Category(){

    }

    public Category(String name, int notes) {
        this.name = name;
        this.notes = notes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNotes() {
        return notes;
    }

    public void setNotes(int notes) {
        this.notes = notes;
    }

    public String toString() {
        return this.getName();
    }
}