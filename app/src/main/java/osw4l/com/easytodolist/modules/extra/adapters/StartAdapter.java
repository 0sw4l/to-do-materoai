package osw4l.com.easytodolist.modules.extra.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.PictureDrawable;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import osw4l.com.easytodolist.R;
import osw4l.com.easytodolist.modules.extra.models.StartItem;

/**
 * Created by osw4l on 29/12/16.
 */

public class StartAdapter extends RecyclerView.Adapter<StartAdapter.StartVH>  {

    OnItemClickListener clickListener;
    private Context context;
    private List<StartItem> items;

    private String title, body_text, text_bottom, color_1, color_2;
    private int icon_button;
    private String background;

    public  StartAdapter(Context context, List<StartItem> items){
        this.context = context;
        this.items = items;
    }

    @Override
    public StartVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.start_item, parent, false);
        return new StartVH(view);
    }


    @Override
    public void onBindViewHolder(StartVH holder, int position) {
        title = items.get(position).getTitle_();
        holder.title.setText(title);
        body_text = items.get(position).getBody_text();
        holder.body_text.setText(body_text);
        text_bottom = items.get(position).getText_bottom();
        holder.text_bottom.setText(text_bottom);
        icon_button = items.get(position).getIcon_button();
        holder.icon_button.setImageResource(icon_button);
        background = items.get(position).getBackground();
        Picasso.with(context)
                .load(background)
                .into(holder.background);
        color_1 = items.get(position).getColor_1();
        holder.part_1.setBackgroundColor(Color.parseColor(color_1));
        color_2 = items.get(position).getColor_2();
        holder.part_2.setBackgroundColor(Color.parseColor(color_2));

        holder.icon_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Hola", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class StartVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title, body_text, text_bottom;
        ImageButton icon_button;
        ImageView background;
        LinearLayout part_1, part_2;


        public StartVH(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title_item);
            body_text = (TextView)itemView.findViewById(R.id.text_body);
            text_bottom = (TextView)itemView.findViewById(R.id.text_bottom);
            icon_button = (ImageButton)itemView.findViewById(R.id.icon_button);
            background = (ImageView)itemView.findViewById(R.id.background_image);
            part_1 = (LinearLayout)itemView.findViewById(R.id.part_1);
            part_2 = (LinearLayout)itemView.findViewById(R.id.part_2);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(final View v) {
            int position = getLayoutPosition();
            Toast.makeText(context, "id : "+position, Toast.LENGTH_SHORT).show();
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }



}
