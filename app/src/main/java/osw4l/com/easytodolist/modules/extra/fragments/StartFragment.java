package osw4l.com.easytodolist.modules.extra.fragments;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import osw4l.com.easytodolist.MainActivity;
import osw4l.com.easytodolist.R;
import osw4l.com.easytodolist.modules.extra.adapters.StartAdapter;
import osw4l.com.easytodolist.modules.extra.models.StartItem;

/**
 * A simple {@link Fragment} subclass.
 */
public class StartFragment extends Fragment {
    MainActivity main;
    StartAdapter adapter;
    RecyclerView recyclerView;
    List<StartItem> items;
    FloatingActionButton fab;
    long contador_de_objetos;

    public StartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_start, container, false);

        this.setActionBarTittle("Bienvenido a To-Do! ");

        Toast.makeText(view.getContext(),
                "Bienvenido a Easy Todo",
                Toast.LENGTH_SHORT).show();

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.show();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment duedateFrag = new HomeFragment();
                FragmentTransaction ft  = getFragmentManager().beginTransaction();
                ft.replace(R.id.fragment, duedateFrag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        setAdapter(view);

        return view;
    }

    public void setActionBarTittle(String t){
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(
                t
        );
    }

    public void setAdapter(View view){
        initializeData();
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_start);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(llm);
        adapter = new StartAdapter(view.getContext(), items);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 0 ||dy<0 && fab.isShown())
                    fab.hide();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                if (newState == RecyclerView.SCROLL_STATE_IDLE){
                    fab.show();
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

    }

    private void initializeData(){
        items = new ArrayList<>();
        items.add(new StartItem(
                "Descubre To-Do!",
                R.drawable.eye_outline,
                "Deslisate para ver mas",
                getResources().getString(R.string.intro_text),
                "https://s30.postimg.org/a77s78l0h/img1.jpg",
                "#80000000",
                "#80000000"
        ));
        items.add(new StartItem(
                "Desde tu SmartPhone o Tablet",
                R.drawable.android_white,
                "MultiDispositivo!",
                "Podras comenzar a organizar tu tiempo desde cualquiera de tus dispositivos que cuente con android 5.0 o superior",
                "https://s30.postimg.org/e2b69t469/img2.jpg",
                "#bb547e15",
                "#bb547e15"
        ));
        items.add(new StartItem(
                "Organizar Tu Tiempo",
                R.drawable.ic_heart,
                "Facil De Usar!",
                "Nunca fue tan facil ordenar tus cosas por hacer, aca lo haras en un momento.",
                "https://s30.postimg.org/swzrnzdr5/img3.jpg",
                "#aaf4612c",
                "#aaf4612c"
        ));
        items.add(new StartItem(
                "Explota al maximo los filtros",
                R.drawable.calendar_clock_white,
                "Filtros!",
                "Sacale el maximo provecho a los filtros de la aplicacion, para saber con mas detalle cuando debes cumplir tus tareas.",
                "https://s30.postimg.org/siyfodtnl/img4.jpg",
                "#a0be9024",
                "#a0be9024"
        ));
        items.add(new StartItem(
                "Categorias !",
                R.drawable.format_list_bulleted_type_white,
                "Crear Una Categoria",
                "To-Do! cuenta con categorias personalizadas para que lleves un orden de tus tareas y puedas filtrarlas mas facil.",
                "https://s30.postimg.org/dlpyndgf5/img5.png",
                "#b71c7abe",
                "#b71c7abe"
        ));
    }

}
