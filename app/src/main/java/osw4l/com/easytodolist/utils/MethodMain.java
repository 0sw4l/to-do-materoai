package osw4l.com.easytodolist.utils;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

import osw4l.com.easytodolist.MainActivity;

/**
 * Created by osw4l on 29/12/16.
 */

public class MethodMain extends Activity {

   public void SnackBar(View view, String s){
       Snackbar.make(view, s, Snackbar.LENGTH_LONG)
               .setAction("Ostia", null).show();
   }


}
